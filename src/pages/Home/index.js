import React from 'react'
import { List } from '../../components'
import './home.css'

function Home() {
    return (
        <div>
            <div className='app-title'>
                <h2 className='text-center'>Pokemon Guide App</h2>
            </div>
            <div>
                <List/>
            </div>
        </div>
    )
}

export default Home
