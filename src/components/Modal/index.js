import React from 'react'
import { Modal, Row, Col, Container } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import './modal.css'

const ModalPopUp = ({ data, ...rest}) => {
    const resData = data ?? []
    const stats = resData?.stats ?? []
    const types = resData?.types ?? []
    const abilities = resData?.abilities ?? []
    const moves = resData?.moves ?? []
    return (
        <div>
            <Modal {...rest}>
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container>
                        <Row>
                            <Col xs="12">
                                <h2>{`#${resData?.id?? ''}`} {resData?.name ?? '-'}</h2>
                                <p className="mb-0"><span className="stats">base exp</span> <span className="statsValue"> {resData?.base_experience?? '-'} </span></p>
                                <p className="mb-0"><span className="stats">weight</span> <span className="statsValue">{resData?.weight?? '-'}</span></p>
                                <p className="mt-0"><span className="stats">height</span> <span className="statsValue">{resData?.height?? '-'}</span></p>
                            </Col>
                        </Row>
                    </Container>
                    <Container>
                        <Row>
                            <Col xm="6">
                                <Row>
                                    <Col sm="6">
                                        <div>
                                            <h2>Stats</h2>
                                        </div>
                                        <div>
                                            <ul>
                                                {
                                                    stats.map((obj, i) => {
                                                        return (
                                                            <li key={i}>
                                                                <span className="stats">{obj.stat.name}: {obj.base_stat}</span>
                                                            </li>
                                                        )
                                                    })
                                                }
                                            </ul>
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm="6">
                                        <div>
                                            <h2>Types</h2>
                                        </div>
                                        <div>
                                            <ul>
                                                {
                                                    types.map((obj, i) =>
                                                        <li key={i}>
                                                            <span className="stats">{obj.type.name}</span>
                                                        </li>
                                                    )
                                                }
                                            </ul>
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm="6">
                                        <div>
                                            <h2>Abilities</h2>
                                        </div>
                                        <div>
                                            <ul>
                                                {
                                                    abilities.map((obj, i) =>
                                                        <li key={i}>
                                                            <span className="stats">{obj.ability.name}</span>
                                                        </li>
                                                    )
                                                }
                                            </ul>
                                        </div>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs="6">
                                <h2>Moves</h2>
                                <ul>
                                    {
                                        moves.map((obj, i) =>
                                            <li key={i}>
                                                <span className="stats">{obj.move.name}</span>
                                            </li>
                                        )
                                    }
                                </ul>
                            </Col>
                        </Row>
                    </Container>
                </Modal.Body>
                {/* <Modal.Footer>
                </Modal.Footer> */}
            </Modal>
        </div>
    )
}

export default ModalPopUp