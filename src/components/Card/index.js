import React from 'react'
import './card.css'

function Card({pokemonName, ...rest}) {
    return (
        <div>
            <div className="pokemon-list">
                <div className="pokemon" {...rest}>
                    <h3>{pokemonName}</h3>
                </div>
            </div>
        </div>
    )
}

export default Card
