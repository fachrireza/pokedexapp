import React, {useState, useEffect} from 'react'
import Axios from 'axios'
import {Row, Container, Button, Col} from 'react-bootstrap'
import {Card, ModalPopUp } from '..'
import './list.css'
import 'bootstrap/dist/css/bootstrap.min.css';


function List() {
    const [pokemonList, setPokemonList] = useState([])
    const [detail, setDetail] = useState([])
    const [next, setNext] = useState()
    const [previous, setPrevious] = useState()
    const [loading, setLoading] = useState(false)
    const [noData, setNoData] = useState(false)
    const [disabledPrev, setDisabledPrev] = useState(true)
    const [disabledNext, setDisabledNext] = useState(false)
    const [modalShow, setModalShow] = useState(false)


    // trigger for modal to show pokemon detail
    const modalToggle = (url) => {
        setDetail([])
        setModalShow(true)
        getPokemonDetail(url) // call function to show pokemon detail
    }

    let screen = window.matchMedia("(max-width: 700px)") //set media screen for using infinite scroll (assume we made 2 different type of user interface mobile and dekstop browser)
    if (screen.matches) {
        //infinite scroll
        window.onscroll = () => {
        if (window.innerHeight + document.documentElement.scrollTop === document.documentElement.offsetHeight) {
            if(!noData) {
                    loadNextPokemonsOnMobileScroll();
                }
            }
        }
    }

    useEffect(() => {
        loadPokemons()
    }, [])

    // get all pokemons when page loaded for first time
    const loadPokemons = () => { 
        setLoading(true);
        setTimeout(() => {
        Axios.get('https://pokeapi.co/api/v2/pokemon')
        .then(response => {
            
            const pokemonItem = response.data.results

            const urlNext = response.data.next //get next url

            const urlPrevious = response.data.previous // get previous url
            
            setPokemonList(pokemonItem)
            setNext(urlNext)
            setPrevious(urlPrevious)
        })
        .catch(error => {
            alert(error);
        })
        .finally(() => {
            setLoading(false)
        })
         }, 1500);
    }

    // get next pokemons by next url on mobile device or small screen
    const loadNextPokemonsOnMobileScroll = () => {
        setLoading(true);
        setDisabledNext(false)
        if (next) {
            setTimeout(() => {
                Axios.get(next)
                .then(response => {
                    // const newPage = page + 1;

                    const pokemonItem = pokemonList.concat(response.data.results)
    
                    const urlNext = response.data.next

                    if(response.data.length===0){
                        setNoData(true);
                    }
                    
                    setPokemonList(pokemonItem)
                    setNext(urlNext)

                })
                .catch(error => {
                    alert(error);
                })
                .finally(() => {
                        setLoading(false)
                    }   
                )     
            }, 1500);
        }
    }

    // get previous pokemons by previous url
    const loadPreviousPokemons = () => {
        setLoading(true);
        setDisabledPrev(false)
        try {
            setTimeout(() => {
                Axios.get(previous)
                    .then(response => {

                        const pokemonItem = response.data.results

                        const urlNext = response.data.next

                        const urlPrevious = response.data.previous

                        //condition when next url for next page is null
                        if (urlNext == null)
                            setDisabledNext(true)
                        setDisabledNext(false)

                        //condition when previous url for previous page is null
                        if (urlPrevious == null)
                            setDisabledPrev(true)
                        setDisabledPrev(false)
                        setPokemonList(pokemonItem)
                        setNext(urlNext)
                        setPrevious(urlPrevious)
                    }).finally(() => {
                        setLoading(false)
                    }
                )   
            }, 1000);
        } catch (error) {
            alert(error);
            setLoading(false)
        }
    }


    useEffect(() => {
        console.log(disabledPrev);
    }, [disabledPrev])

    // get next pokemons by next url
    const loadNextPokemons = () => {
        setLoading(true);
        setDisabledNext(false)
        try {
            setTimeout(() => {
                Axios.get(next)
                    .then(response => {

                        const pokemonItem = response.data.results

                        const urlNext = response.data.next

                        const urlPrevious = response.data.previous

                        //condition when next url for next page is null
                        if (urlNext == null)
                            setDisabledNext(true)
                        setDisabledNext(false)

                        //condition when previous url for previous page is null
                        if (urlPrevious == null)
                            setDisabledPrev(true)
                        setDisabledPrev(false)
                        setPokemonList(pokemonItem)
                        setNext(urlNext)
                        setPrevious(urlPrevious)
                    }).finally(() => {
                        setLoading(false)
                    }
                )
            }, 1000);
        } catch (error) {
            alert(error);
            setLoading(false)
        }
    }

    
    //Get pokemon detail
    const getPokemonDetail = (urlDetail) => {
        try {
            Axios.get(urlDetail)
                .then(response => {
                    setDetail(response.data)
                    setLoading(false)
                }).finally(() => {
                    setLoading(false)
                }
            )
        } catch (error) {
            alert(error);
            setLoading(false)
        }
    }
    
    return (
        <div className='pokemon-item'>
            <Container fluid>
                <Container>
                    <Row>
                        <Col sm={12}>
                        {
                            pokemonList.map((pokemon, i) => (
                                <Card 
                                    key={i}
                                    pokemonName={pokemon.name}
                                    id={pokemon.name} 
                                    url={pokemon.url}
                                    onClick={() => modalToggle(pokemon.url)}
                                />
                            ))
                        }
                        </Col>
                    </Row>
                    <Row>
                        {loading ?  <div className="text-center loading-text">Loading ...</div> : "" }
                        {noData ?  <div className="text-center loading-text">No More Data ...</div> : "" }
                    </Row>
                    {
                        pokemonList.length > 0 ?
                        <Row className="mt-1 mb-5 buttonNavigate">
                            <Col xs={6} className="d-grid gap-2 mb-1">
                                <Button className="d-none d-sm-block navigateBtn" variant="success outline-warning" size="lg" title="Previous" onClick={loadPreviousPokemons} disabled={disabledPrev}>Previous</Button>
                            </Col>
                            <Col xs={6} className="d-grid gap-2 mb-1">
                                <Button className="d-none d-sm-block navigateBtn" variant="success outline-warning" size="lg" title="Next" onClick={loadNextPokemons} disabled={disabledNext}>Next</Button>
                            </Col>
                        </Row>       
                        :
                        <div></div> 
                    }
                </Container>
            </Container>
            <ModalPopUp
                size="lg"
                alt='image-preview'
                height='auto'
                width='100%'
                show={modalShow}
                onHide={()=>setModalShow(false)}
                aria-labelledby="contained-modal-title-vcenter"
                centered
                data={detail}
            />
            
        </div>
    )
}

export default List
