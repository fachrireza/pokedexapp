import React from 'react'
import'./button.css'

function Button({title, ...rest}) {
    return (
        
            <button className="button" {...rest}>{title}</button>
        
    )
}

export default Button